import socket

host = '127.0.0.1'  # IP address ของเซิร์ฟเวอร์
port = 55423       # Port ของเซิร์ฟเวอร์

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("Socket successfully created")

server_socket.bind((host, port))
print("Socket binded to %s:%s" % (host,port))

server_socket.listen(1)
print("Server is listening...")

file_path = r"D:\Network\client-server\a.txt.txt"
with open(file_path, "r") as file:
    file_content = file.read()

while True:
    client_socket, client_address = server_socket.accept()
    print("Connected by:", client_address)
    
    client_socket.sendall(file_content.encode())
    
    client_socket.close()